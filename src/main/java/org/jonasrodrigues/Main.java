package org.jonasrodrigues;

import org.jonasrodrigues.assets.services.Collectors;
import org.jonasrodrigues.assets.services.ParallelStream;
import org.jonasrodrigues.assets.services.Reduce;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {

        System.out.println("REDUCES: ");
        Reduce reduce = new Reduce();
        reduce.reduce();
        System.out.println();

        System.out.println("COLLECTORS: ");
        Collectors collectors = new Collectors();
        collectors.collectorsWithGrouping();
        System.out.println();
        collectors.collectToList();
        System.out.println();
        collectors.printingOptional();

        System.out.println("PERFORMANCE - PARALLELS STREAM: ");
        ParallelStream parallelStream = new ParallelStream();
        parallelStream.getAvailableCores();
        parallelStream.filterInParallel();
        parallelStream.filterInParallelVerbose();
        System.out.println();

        System.out.println("PERFORMANCE - USE Collectors.toList OR direct implements... : ");
        parallelStream.toListVsManualImplementation();
        System.out.println();

    }

}