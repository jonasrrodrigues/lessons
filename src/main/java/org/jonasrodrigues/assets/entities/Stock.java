package org.jonasrodrigues.assets.entities;

import org.jonasrodrigues.assets.entities.enums.AssetType;
import org.jonasrodrigues.assets.entities.enums.Convertibility;

import java.math.BigDecimal;

public class Stock extends Asset  {

    private Stock(AssetType assetType, Convertibility convertibility) {
        super(assetType, convertibility);
    }

    public static class Builder implements org.jonasrodrigues.assets.entities.Builder<Stock, Builder> {

        private Stock entity;

        public Builder(Stock entity) {
            this.entity = entity;
        }

        public static Builder create() {
            return new Builder(new Stock(AssetType.STOCK, Convertibility.CURRENT_ASSET));
        }

        public Builder value(BigDecimal value) {
            entity.setValue(value);
            return this;
        }

        public Builder name(String name) {
            entity.setName(name);
            return this;
        }

        public Builder quantity(int quantity) {
            entity.setQuantity(quantity);
            return this;
        }
        @Override
        public Stock build() {
            return entity;
        }
    }
}
