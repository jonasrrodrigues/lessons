package org.jonasrodrigues.assets.entities;

import lombok.Getter;
import lombok.Setter;
import org.jonasrodrigues.assets.entities.enums.AssetType;
import org.jonasrodrigues.assets.entities.enums.Convertibility;

import java.math.BigDecimal;

@Getter
public abstract class Asset {


    public Asset(AssetType assetType, Convertibility convertibility) {
        this.assetType = assetType;
        this.convertibility = convertibility;
    }

    @Setter
    private BigDecimal value;

    private AssetType assetType;

    private Convertibility convertibility;

    @Setter
    private int quantity;

    @Setter
    private String name;

    @Override
    public String toString() {
        return name + " value R$" + value;
    }
}
