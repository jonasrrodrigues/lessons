package org.jonasrodrigues.assets.entities.enums;

public enum AssetType {
    STOCK, REIT, REAL_STATE, CASH
}
