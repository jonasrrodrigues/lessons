package org.jonasrodrigues.assets.entities.enums;

public enum Convertibility {
    CURRENT_ASSET,
    FIXED_ASSET
}
