package org.jonasrodrigues.assets.entities;

public interface Builder<T, B> {

    T build();
}
