package org.jonasrodrigues.assets.utils;

import org.jonasrodrigues.assets.entities.Asset;
import org.jonasrodrigues.assets.entities.Stock;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class UtilsCSV {

    public static final String FILE_PATH = "/home/jonas.rocha/projects/assets/src/main/java/org/jonasrodrigues/assets.csv";

    public static List<Asset> importCSV(String csvFilePath) {
        try (Stream<String> stream = Files.lines(Paths.get(csvFilePath))) {
            return stream.map(
                    l -> {
                        final String[] fields = l.split(",");
                        return Stock.Builder.create()
                                .name(fields[0])
                                .quantity(Integer.valueOf(fields[1]))
                                .value(new BigDecimal(fields[2]))
                                .build();
                    }
            ).collect(Collectors.toList());
        } catch (IOException e) {
            return new ArrayList<>();
        }
    }
}
