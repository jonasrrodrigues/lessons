package org.jonasrodrigues.assets.utils;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

public final class CollectToList {

    public static class CollectorImpl<T, A, R> implements Collector<T, A, R> {

        public static final Set<Characteristics> CHARACTERISTICS = EnumSet.of(Collector.Characteristics.IDENTITY_FINISH);

        private final Supplier<A> supplier;

        private final BiConsumer<A, T> accumulator;

        private final BinaryOperator<A> combiner;

        private final Function<A, R> finisher;

        private final Set<Characteristics> characteristics;

        private static <I, R> Function<I, R> casting() {
            return i -> (R) i;
        }

        public CollectorImpl(Supplier<A> supplier, BiConsumer<A, T> accumulator, BinaryOperator<A> combiner, Set<Characteristics> characteristics) {
            this(supplier, accumulator, combiner, casting(), characteristics);
        }

        public CollectorImpl(Supplier<A> supplier, BiConsumer<A, T> accumulator, BinaryOperator<A> combiner, Function<A, R> finisher, Set<Characteristics> characteristics) {
            this.supplier = supplier;
            this.accumulator = accumulator;
            this.combiner = combiner;
            this.finisher = finisher;
            this.characteristics = characteristics;
        }

        @Override
        public Supplier<A> supplier() {
            return supplier;
        }

        @Override
        public BiConsumer<A, T> accumulator() {
            return accumulator;
        }

        @Override
        public BinaryOperator<A> combiner() {
            return combiner;
        }

        @Override
        public Function<A, R> finisher() {
            return finisher;
        }

        @Override
        public Set<Characteristics> characteristics() {
            return characteristics;
        }
    }

    public static <T> Collector<T, ?, List<T>> toGetterMethods() {
            return new CollectorImpl<>(
                    ArrayList::new,
                    toGetter(),
                    (left, right) -> {left.addAll(right); return left;} ,
                    CollectorImpl.CHARACTERISTICS
            );
        }

    private static <T> BiConsumer<ArrayList<Object>, T> toGetter() {
        return (list, elem) -> {
            String elementStr =  elem.toString();
            StringBuilder builder = new StringBuilder("get");
            builder.append(String.valueOf(elementStr.charAt(0)).toUpperCase());
            builder.append(elem.toString(), 1, elementStr.length());
            list.add(builder.toString());
        };
    }
}
