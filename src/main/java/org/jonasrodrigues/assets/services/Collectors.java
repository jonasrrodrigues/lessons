package org.jonasrodrigues.assets.services;

import org.jonasrodrigues.assets.entities.Asset;
import org.jonasrodrigues.assets.utils.CollectToList;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static org.jonasrodrigues.assets.utils.UtilsCSV.FILE_PATH;
import static org.jonasrodrigues.assets.utils.UtilsCSV.importCSV;

public class Collectors {

    public void collectToList() {
        System.out.println("Implementation of Collect equivalent to Collectors.toList...");
        Field[] fields = Asset.class.getDeclaredFields();
        ArrayList<String> collect = Stream.of(fields)
                .map(field -> field.getName())
                .collect(ArrayList::new, ArrayList::add, ArrayList::addAll);
        // it's equal
       /* .collect(() -> new ArrayList<>(),
                (list, element) -> list.add(element),
                (listOne, listTwo) -> listOne.addAll(listTwo));
       */
        collect.forEach(System.out::println);
        System.out.println();

        System.out.println("Implements an personalized collector to generate 'get' methods of fields...");
        Stream.of(fields)
                .map(Field::getName)
                .collect(CollectToList.toGetterMethods())
                .forEach(System.out::println);
    }

    public void collectorsWithGrouping() {
        List<Asset> assets = importCSV(FILE_PATH);

        System.out.println("Find first lowest 30...");
        assets.stream()
                .filter(a -> BigDecimal.valueOf(30).compareTo(a.getValue()) > 0)
                .findFirst()
                .ifPresent(System.out::println);
        System.out.println();

        System.out.println("Grouping value by stock name...");
        assets.stream()
                .collect(java.util.stream.Collectors.groupingBy(Asset::getName,
                        java.util.stream.Collectors.summingDouble(this::sumTotalValueByAsset)))
                .forEach( (name, value) -> System.out.println(name + " value R$" + value));
    }

    public void printingOptional() {
        List<Asset> assets = importCSV(FILE_PATH);

        System.out.println("Printing an optional value...");
        assets.stream()
                .filter(a -> BigDecimal.valueOf(30).compareTo(a.getValue()) > 0)
                .findFirst()
                .ifPresent(System.out::println);

    }

    private double sumTotalValueByAsset(Asset asset) {
        return asset.getValue().multiply(new BigDecimal(asset.getQuantity())).doubleValue();
    }

}
