package org.jonasrodrigues.assets.services;

import java.util.ArrayList;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class ParallelStream {
    public void filterInParallel(){
        //Without parallel
        long startSingleTimeLapse = System.nanoTime();
        IntStream.range(0, 1000000000)
                .filter( n -> n % 2 == 0)
                .count();
        long singleTimeLapse = System.nanoTime() - startSingleTimeLapse;
        System.out.println(String.format("Total time execution in single thread: %s ms ", singleTimeLapse / 1000000));

        //With parallel
        long startMultiTimeLapse = System.nanoTime();
        IntStream.range(0, 1000000000)
                .parallel()
                .filter( n -> n % 2 == 0)
                .count();
        long multiTimeLapse = System.nanoTime() - startMultiTimeLapse;
        System.out.println(String.format("Total time execution in multi thread: %s ms ", multiTimeLapse / 1000000));

    }

    public void filterInParallelVerbose(){

        System.out.println("Order of execution does not linear...");
        long startMultiTimeLapse = System.nanoTime();
        IntStream.range(0, 100)
                .parallel()
                .limit(20)
                .filter( n -> {
                    System.out.println(n);
                    return n % 2 == 0;
                })
                .count();

        long multiTimeLapse = System.nanoTime() - startMultiTimeLapse;
        System.out.println(String.format("Total time execution in multi thread: %s ms ", multiTimeLapse / 1000000));

    }

    public void getAvailableCores() {
        System.out.println("Total of available cores...");
        int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("Your available cores is " + processors);
    }

    public void toListVsManualImplementation() {

        long start = System.nanoTime();
        System.out.print("Total time execution with manual implementation...");
        IntStream.range(0, 10000000)
                .filter( n -> n % 2 == 0)
                .boxed()
                .collect(ArrayList::new, ArrayList::add, ArrayList::addAll);
        System.out.println((System.nanoTime() - start) / 1000000 + "ms");

        start = System.nanoTime();
        System.out.print("Total time execution with toList implementation...");
        IntStream.range(0, 10000000)
                .filter( n -> n % 2 == 0)
                .boxed()
                .collect(Collectors.toList());
        System.out.println((System.nanoTime() - start) / 1000000 + "ms");

    }

}
