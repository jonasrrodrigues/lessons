package org.jonasrodrigues.assets.services;

import org.jonasrodrigues.assets.entities.Asset;

import java.math.BigDecimal;
import java.util.List;

import static org.jonasrodrigues.assets.utils.UtilsCSV.FILE_PATH;
import static org.jonasrodrigues.assets.utils.UtilsCSV.importCSV;

public class Reduce {

    public void reduce() {
        List<Asset> assets = importCSV(FILE_PATH);

        System.out.println("Return the Sum of Microsoft shares: ");
        Integer microsoftQuantityTotal = assets.stream()
                .filter(a -> a.getName().equals("Microsoft"))
                .map(Asset::getQuantity)
                .reduce(0, Integer::sum);
        System.out.println("Your total of microsoft stocks is: " +  microsoftQuantityTotal);
        System.out.println();

        System.out.println("Return the sum of the investment amount in Microsoft: ");
        BigDecimal totalValueOfMicrosofStocks = assets.stream()
                .filter(a -> a.getName().equals("Microsoft"))
                .map(a -> a.getValue().multiply(new BigDecimal(a.getQuantity())))
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        System.out.println("Your value total of microsoft stocks is: R$" +  totalValueOfMicrosofStocks);
        System.out.println();


        System.out.println("Lowest priced: ");
        Double minStockPrice = assets.stream()
                .map(asset -> asset.getValue().doubleValue())
                .reduce(Double.MAX_VALUE, (a, b) -> Math.min(a, b));
        System.out.println("The lowest price is: R$" + minStockPrice);
        System.out.println();


        System.out.println("Greater price: ");
        Double maxStockPrice = assets.stream()
                .map(asset -> asset.getValue().doubleValue())
                .reduce(Double.MIN_VALUE, (a, b) -> Math.max(a, b));
        System.out.println("The Greater price is R$" + maxStockPrice);
        System.out.println();

    }

}
